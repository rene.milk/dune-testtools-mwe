#include <config.h>
#include <dune/common/parallel/mpihelper.hh>
#include "static_test.hh"

int main(int argc, char** argv) {
  try {
    // Maybe initialize MPI
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    return check_mwe<FIRST_TYPE, SECOND_TYPE>();
  } catch (Dune::Exception& e) {
    std::cerr << "Dune reported error: " << e << std::endl;
  } catch (...) {
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
