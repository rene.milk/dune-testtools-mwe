#ifndef STATIC_TEST_HH
#define STATIC_TEST_HH

#include <dune/testtools-mwe/library.hh>

template <class FirstType, class SecondType>
int check_mwe() {
  using namespace Dune::Testtools;
  const auto is_ok = a_function_template_to_call(FirstType(), SecondType())
      && a_library_function_to_call("foo", 1);
  return is_ok ? 0 : 127;
}

#endif // STATIC_TEST_HH
