#ifndef DUNE_TESTTOOLS_MWE_HH
#define DUNE_TESTTOOLS_MWE_HH

#include <string>
#include <type_traits>

namespace Dune {
namespace Testtools {

template <class FirstType, class SecondType>
bool a_function_template_to_call(FirstType /*first*/, SecondType /*second*/) {
  return std::is_integral<FirstType>::value
      || (std::is_same<FirstType, SecondType>::value && std::is_same<FirstType, std::string>::value);
}

bool a_library_function_to_call(std::string, int);

}
}

#endif // DUNE_TESTTOOLS_MWE_HH
