#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>

#include <dune/testtools-mwe/library.hh>

int main(int argc, char** argv) {
  using namespace Dune::Testtools;
  try {
    // Maybe initialize MPI
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    return a_function_template_to_call(1l, 0u) || a_library_function_to_call("foo", 1);
  } catch (Dune::Exception& e) {
    std::cerr << "Dune reported error: " << e << std::endl;
  } catch (...) {
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
